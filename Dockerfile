FROM openjdk:17-jdk-slim

ADD target/githubaction.jar githubaction.jar

ENTRYPOINT ["java", "-jar","githubaction.jar"]